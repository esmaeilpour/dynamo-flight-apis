const sinon = require('sinon');
const request = require('test/support/request');

describe('API :: POST /v1/flights', () => {
  beforeEach(() => {
    sinon.restore();
  });

  it('Unauthorized', async () => {
    await request().post('/v1/flights').expect(401);
  });
});
