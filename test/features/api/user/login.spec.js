const sinon = require('sinon');
const request = require('test/support/request');

describe('API :: POST /v1/users/login', () => {
  beforeEach(() => {
    sinon.restore();
  });

  it('Unauthorized', async () => {
    await request().post('/v1/users/login').expect(401);
  });
});
