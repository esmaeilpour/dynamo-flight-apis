const { expect } = require('chai');
const SignUpUser = require('src/app/user/SignUpUser');
const sinon = require('sinon');

describe('App :: User :: SignUpUser', () => {
  let signUp;
  let successSpy = sinon.spy();
  let failSpy = sinon.spy();

  context('create user', () => {
    beforeEach(() => {
      const MockRepos = {
        addUser: (username, password) =>
          Promise.resolve({
            _id: '1',
            username,
            password,
          }),
      };

      signUp = new SignUpUser({
        userRepository: MockRepos,
      });

      signUp.on(signUp.outputs.SUCCESS, successSpy);
      signUp.on(signUp.outputs.BAD_REQUEST, failSpy);
    });

    it('emits SUCCESS', async () => {
      await signUp.execute({
        username: 'foo',
        password: '1234',
      });

      const spyCall = successSpy.getCall(0);

      expect(successSpy.callCount).to.equal(1);
      expect(spyCall.args[0]._id).to.be.equal('1');
    });

    it('emits BAD_REQUEST', async () => {
      await signUp.execute({
        username: 'foo',
        password: '123',
      });

      expect(failSpy.callCount).to.equal(1);
    });
  });
});
