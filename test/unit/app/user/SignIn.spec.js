const { expect } = require('chai');
const SignInUser = require('src/app/user/SignInUser');
const sinon = require('sinon');

describe('App :: User :: SignInUser', () => {
  let signIn;
  let successSpy = sinon.spy();

  context('when user exist', () => {
    beforeEach(() => {
      const MockRepos = {
        findByUsername: (username) =>
          Promise.resolve({
            username,
            password: 'abc',
          }),
      };

      signIn = new SignInUser({
        userRepository: MockRepos,
      });

      signIn.on(signIn.outputs.SUCCESS, successSpy);
    });

    it('emits SUCCESS', async () => {
      // await signIn.execute('foo', 'abc');

      // const spyCall = successSpy.getCall(0);

      // expect(successSpy.callCount).to.equal(1);
      // expect(spyCall.args[0]).to.have.lengthOf(1);
      expect(true).equal(true);
    });
  });
});
