const { expect } = require('chai');
const sinon = require('sinon');
const SearchFlights = require('src/app/flight/SearchFlights');
const config = require('config/environments/test');

describe('App :: Flight :: SearchFlights', () => {
  let searchFlight;
  let successSpy = sinon.spy();

  beforeEach(() => {
    const MockRepos = {
      searchFlights: () =>
        Promise.resolve([{ name: 'foo' }]),
      countFlights: () => Promise.resolve(1),
    };

    searchFlight = new SearchFlights({
      flightRepository: MockRepos,
      config,
    });

    searchFlight.on(searchFlight.outputs.SUCCESS, successSpy);
  });

  it('emits SUCCESS', async () => {
    await searchFlight.execute({
      name: 'foo',
    });
    const spyCall = successSpy.getCall(0);

    expect(successSpy.callCount).to.equal(1);
    expect(spyCall.args[0].flights).to.have.lengthOf(1);
  });
});
