const { expect } = require('chai');
const UpdateFlight = require('src/app/flight/UpdateFlight');
const sinon = require('sinon');

describe('App :: Flight :: UpdateFlight', () => {
  let updateFlight;
  let successSpy = sinon.spy();
  let failSpy = sinon.spy();

  beforeEach(() => {
    const MockRepos = {
      updateFlight: (id, flight) => Promise.resolve(id ? flight : undefined),
    };

    updateFlight = new UpdateFlight({
      flightRepository: MockRepos,
    });

    updateFlight.on(updateFlight.outputs.SUCCESS, successSpy);
    updateFlight.on(updateFlight.outputs.NOT_FOUND, failSpy);
  });

  it('emits SUCCESS', async () => {
    await updateFlight.execute(1, {
      name: 'foo',
      number: 'F1',
      scheduledDate: '2012',
      eta: '2012',
      departure: 'foo',
      destination: 'bar',
      fare: 1,
      duration: 10,
    });
    expect(successSpy.callCount).to.equal(1);
  });

  it('emits NOT_FOUND', async () => {
    await updateFlight.execute(0, {
      name: 'foo',
      number: 'F1',
      scheduledDate: '2012',
      eta: '2012',
      departure: 'foo',
      destination: 'bar',
      fare: 1,
      duration: 10,
    });
    expect(failSpy.callCount).to.equal(1);
  });
});
