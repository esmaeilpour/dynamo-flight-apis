const { expect } = require('chai');
const CreateFlight = require('src/app/flight/CreateFlight');
const sinon = require('sinon');

describe('App :: Flight :: CreateFlight', () => {
  let createFlight;
  let successSpy = sinon.spy();
  let failSpy = sinon.spy();

  beforeEach(() => {
    const MockRepos = {
      createFlight: (flight) => Promise.resolve({ ...flight }),
    };

    createFlight = new CreateFlight({
      flightRepository: MockRepos,
    });

    createFlight.on(createFlight.outputs.SUCCESS, successSpy);
    createFlight.on(createFlight.outputs.BAD_REQUEST, failSpy);
  });

  it('emits SUCCESS', async () => {
    await createFlight.execute({
      name: 'foo',
      number: 'F1',
      scheduledDate: '2012',
      eta: '2012',
      departure: 'foo',
      destination: 'bar',
      fare: 1,
      duration: 10,
    });
    expect(successSpy.callCount).to.equal(1);
  });

  it('emits BAD_REQUEST', async () => {
    await createFlight.execute({
      name: 'foo'
    });
    expect(failSpy.callCount).to.equal(1);
  });
});
