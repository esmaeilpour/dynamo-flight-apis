const { expect } = require('chai');
const RemoveFlight = require('src/app/flight/RemoveFlight');
const sinon = require('sinon');

describe('App :: Flight :: RemoveFlight', () => {
  let removeFlight;
  let successSpy = sinon.spy();
  let failSpy = sinon.spy();

  beforeEach(() => {
    const MockRepos = {
      removeFlight: (id) => Promise.resolve(id),
    };

    removeFlight = new RemoveFlight({
      flightRepository: MockRepos,
    });

    removeFlight.on(removeFlight.outputs.SUCCESS, successSpy);
    removeFlight.on(removeFlight.outputs.NOT_FOUND, failSpy);
  });

  it('emits SUCCESS', async () => {
    await removeFlight.execute(1);
    expect(successSpy.callCount).to.equal(1);
  });

  it('emits NOT_FOUND', async () => {
    await removeFlight.execute(0);
    expect(failSpy.callCount).to.equal(1);
  });
});
