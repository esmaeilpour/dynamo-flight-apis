const { expect } = require('chai');
const Flight = require('src/domain/flight/Flight');

describe('Domain :: Flight', () => {
  it('Creating new flight entity', () => {
    const flight = new Flight({ name: 'foo' });

    expect(flight.name).to.be.equal('foo');
  });

  it('Is scheduled date correct', () => {
    const flight = new Flight({ scheduledDate: '2012' });

    expect(flight.scheduledDate.toISOString()).to.be.equal(new Date('2012').toISOString());
  });
});
