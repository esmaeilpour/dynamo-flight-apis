const { expect } = require('chai');
const User = require('src/domain/user/User');

describe('Domain :: User', () => {
  it('Creating new user entity', () => {
    const user = new User({ username: '123', password: 'abc' });

    expect(user.username).to.be.equal('123');
  });

  it('Password is matched', () => {
    const user = new User({ username: '123', password: 'abc' });

    expect(user.isPasswordMatched('abc')).to.be.equal(true);
  });

  it('Password is strong', () => {
    const user = new User({ username: '123', password: 'abcd' });

    expect(user.isStrongPassword()).to.be.equal(true);
  });

  it('Password is not strong', () => {
    const user = new User({ username: '123', password: 'abc' });

    expect(user.isStrongPassword()).to.be.equal(false);
  });
});
