const { expect } = require('chai');
var Datastore = require('nedb-promises');
const Flight = require('src/domain/flight/Flight');
const InMemoryRepository = require('src/infra/flight/InMemoryRepository');

describe('Infra :: Flight :: InMemoryRepo', () => {
  let repo;
  let flightData = {
    name: 'foo',
    number: 'F1',
    scheduledDate: '2012',
    eta: '2012',
    departure: 'foo',
    destination: 'bar',
    fare: 1,
    duration: 10,
  };

  beforeEach(() => {
    const memoryDB = {
      flights: new Datastore(),
    };

    repo = new InMemoryRepository({
      memoryDB,
    });
  });

  it('createFlight', async () => {
    const flight = await repo.createFlight(new Flight(flightData));
    expect(flight.name).to.equal('foo');
  });

  it('updateFlight', async () => {
    const flight = await repo.createFlight(new Flight(flightData));

    const updatedFlight = await repo.updateFlight(
      flight.id,
      new Flight({ ...flightData, name: 'foo2' })
    );
    expect(updatedFlight.name).to.equal('foo2');
  });

  it('countFlights', async () => {
    await repo.createFlight(new Flight(flightData));
    await repo.createFlight(new Flight(flightData));

    const count = await repo.countFlights({
      name: 'foo',
    });
    expect(count).to.equal(2);
  });

  it('searchFlights', async () => {
    await repo.createFlight(new Flight(flightData));
    await repo.createFlight(new Flight(flightData));

    const flights = await repo.searchFlights({
      name: 'foo',
    });
    expect(flights.length).to.equal(2);
  });
});
