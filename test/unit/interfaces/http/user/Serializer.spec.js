const { expect } = require('chai');
const UserSerializer = require('src/interfaces/http/user/Serializer');
const User = require('src/domain/user/User');

describe('Interfaces :: HTTP :: User :: UserSerializer', () => {
  it('is able to serialize user entity instances', () => {
    const user = new User({
      username: 'foo',
      token: 'bar',
    });
    const serializedUser = UserSerializer.serialize(user);

    expect(serializedUser).to.eql({
      username: 'foo',
      token: 'bar',
    });
  });

  it('ignores extra attributes', () => {
    const serializedUser = UserSerializer.serialize({
      username: 'foo',
      token: 'bar',
      unknown: 'x',
    });

    expect(serializedUser).to.eql({
      username: 'foo',
      token: 'bar',
    });
  });
});
