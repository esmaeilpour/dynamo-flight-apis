const UserMapper = require('./UserMapper');

class UserInMemRepository {
  constructor({ memoryDB }) {
    this.db = memoryDB.users;
  }

  async addUser(user) {
    const doc = await this.db.insert(UserMapper.toDatabase(user));
    return UserMapper.toEntity(doc);
  }

  async findByUsername(username) {
    const user = await this.db.findOne({ username });
    return user && UserMapper.toEntity(user);
  }
}

module.exports = UserInMemRepository;
