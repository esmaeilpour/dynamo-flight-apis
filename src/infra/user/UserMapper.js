const User = require('src/domain/user/User');

const UserMapper = {
  toEntity( dataValues ) {
    const { _id, username, password } = dataValues;

    return new User({ id: _id, username, password });
  },

  toDatabase(survivor) {
    const { username, password } = survivor;

    return { username, password };
  }
};

module.exports = UserMapper;
