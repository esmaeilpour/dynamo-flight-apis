var Datastore = require('nedb-promises');

const db = {
  users: new Datastore({ filename: '../../data/users.db', autoload: true }),
  flights: new Datastore({ filename: '../../data/flights.db', autoload: true }),
};

db.users.ensureIndex({ fieldName: 'username', unique: true });

module.exports = db;
