const FlightMapper = require('./FlightMapper');

class FlightInMemRepository {
  constructor({ memoryDB }) {
    this.db = memoryDB.flights;
  }

  async createFlight(flight) {
    const doc = await this.db.insert(FlightMapper.toDatabase(flight));
    return FlightMapper.toEntity(doc);
  }

  async updateFlight(id, flight) {
    const updated = FlightMapper.toDatabase(flight);
    const updatedDoc = await this.db.update(
      { _id: id },
      { $set: updated },
      { multi: false, returnUpdatedDocs: true }
    );
    // console.log('@affectedDocuments', numAffected);
    return FlightMapper.toEntity(updatedDoc);
  }

  async removeFlight(_id) {
    return await this.db.remove({ _id }, { multi: false });
  }

  async countFlights(queryParams) {
    return await this.db.count({
      $and: this.normalizeQueryParams(queryParams),
    });
  }

  async searchFlights(queryParams, offset = 0, perPage = 10) {
    return await this.db
      .find({
        $and: this.normalizeQueryParams(queryParams),
      })
      .skip(offset * perPage)
      .limit(perPage)
      .exec();
  }

  normalizeQueryParams(queryParams) {
    return Object.getOwnPropertyNames(queryParams).map((name) => {
      if (queryParams[name]) {
        if (name === 'scheduledDate' || name === 'eta') {
          return { [name]: new Date(queryParams[name]).toISOString() };
        }
        return { [name]: queryParams[name] };
      }
      return {};
    });
  }
}

module.exports = FlightInMemRepository;
