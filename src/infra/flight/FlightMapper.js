const Flight = require('src/domain/flight/Flight');

const FlightMapper = {
  toEntity(dataValues) {
    const {
      _id,
      name,
      number,
      scheduledDate,
      eta,
      departure,
      destination,
      fare,
      duration,
    } = dataValues;

    return new Flight({
      id: _id,
      name,
      number,
      scheduledDate,
      eta,
      departure,
      destination,
      fare,
      duration,
    });
  },

  toDatabase(survivor) {
    const {
      name,
      number,
      scheduledDate,
      eta,
      departure,
      destination,
      fare,
      duration,
    } = survivor;

    return {
      name,
      number,
      scheduledDate: scheduledDate.toISOString(),
      eta: eta.toISOString(),
      departure,
      destination,
      fare,
      duration,
    };
  },
};

module.exports = FlightMapper;
