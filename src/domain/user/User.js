const { attributes } = require('structure');
const validator = require('validator');

const User = attributes({
  id: Number,
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  token: {
    type: String,
  },
})(
  class User {
    setToken(token) {
      this.token = token;
    }
    isStrongPassword() {
      return validator.isStrongPassword(this.password, {
        minLength: 4,
        minLowercase: 0,
        minUppercase: 0,
        minNumbers: 0,
        minSymbols: 0,
      });
    }
    isPasswordMatched(password) {
      return this.password === password;
    }
  }
);

module.exports = User;
