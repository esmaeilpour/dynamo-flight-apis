const { attributes } = require('structure');

const Flight = attributes({
  id: String,
  name: {
    type: String,
    required: true,
  },
  number: {
    type: String,
    required: true,
  },
  scheduledDate: {
    type: Date,
    required: true,
  },
  eta: {
    type: Date,
    required: true,
  },
  departure: {
    type: String,
    required: true,
  },
  destination: {
    type: String,
    required: true,
  },
  fare: {
    type: Number,
    required: true,
  },
  duration: {
    type: Number,
    required: true,
  },
})(class Flight {});

module.exports = Flight;
