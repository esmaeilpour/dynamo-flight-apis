const { Router } = require('express');
const statusMonitor = require('express-status-monitor');
const cors = require('cors');
const bodyParser = require('body-parser');
const compression = require('compression');
const methodOverride = require('method-override');
const jwt = require('express-jwt');
const Status = require('http-status');
const controller = require('./utils/createControllerRoutes');

module.exports = ({
  config,
  containerMiddleware,
  loggerMiddleware,
  errorHandler,
  rateLimiter,
}) => {
  const router = Router();

  /* istanbul ignore if */
  if (config.env === 'development') {
    router.use(statusMonitor());
  }

  /* istanbul ignore if */
  if (config.env !== 'test') {
    router.use(loggerMiddleware);
  }

  const apiRouter = Router();

  apiRouter
    .use(methodOverride('X-HTTP-Method-Override'))
    .use(cors())
    .use(bodyParser.json())
    .use(compression())
    .use(containerMiddleware);

  /*
   * Add your API routes here
   *
   * You can use the `controllers` helper like this:
   * apiRouter.use('/users', controller(controllerPath))
   *
   * The `controllerPath` is relative to the `interfaces/http` folder
   */



  apiRouter.use('/users', controller('user/UserController'));
  apiRouter.use(
    '/flights',
    jwt({ secret: config.jwt.secret, algorithms: [config.jwt.algorithm] }),
    controller('flight/FlightController')
  );

  apiRouter.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
      res.status(Status.UNAUTHORIZED).send({
        type: err.name,
      });
    } else {
      next();
    }
  });

  router.use('/v1', [rateLimiter(config.rate_limit)], apiRouter);



  router.use(errorHandler);

  return router;
};
