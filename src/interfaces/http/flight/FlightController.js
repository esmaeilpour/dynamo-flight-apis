const { Router } = require('express');
const { inject } = require('awilix-express');
const Status = require('http-status');

/**
 * @typedef Flight
 * @property {string} name.required
 * @property {string} number.required
 * @property {string} scheduledDate.required
 * @property {string} eta.required
 * @property {string} departure.required
 * @property {string} destination.required
 * @property {integer} fare.required
 * @property {integer} duration.required
 */

/**
 * @typedef Error
 * @property {string} type.required
 */

/**
 * @typedef Response
 * @property {[integer]} code
 */

const FlightController = {
  get router() {
    const router = Router();

    router.use(inject('flightsSerializer'));

    router.post('/', inject('createFlight'), this.create);

    router.put('/:id', inject('updateFlight'), this.update);

    router.delete('/:id', inject('removeFlight'), this.remove);

    router.get('/search', inject('searchFlight'), this.search);

    return router;
  },

  /**
   * The endpoint for creating a new flight
   * @route POST /flights
   * @param {Flight.model} flight.body.required
   * @group flights - Operations about flights
   * @operationId CreateFlight
   * @returns {Flight.model} 200 - flight info
   * @returns {Error.model} 400 - Operation failed
   * @security JWT
   */

  create(req, res, next) {
    const { createFlight, flightsSerializer } = req;

    const { SUCCESS, ERROR, BAD_REQUEST } = createFlight.outputs;

    createFlight
      .on(SUCCESS, (flight) => {
        res.status(Status.OK).json(flightsSerializer.serializeFlight(flight));
      })
      .on(BAD_REQUEST, (error) => {
        res.status(Status.BAD_REQUEST).json({
          type: 'InvalidParams',
          details: error.details,
        });
      })
      .on(ERROR, next);

    createFlight.execute(req.body);
  },

  /**
   * The endpoint for updating flight information
   * @route PUT /flights/{id}
   * @param {string} id.path.required
   * @param {Flight.model} flight.body.required
   * @group flights - Operations about flights
   * @operationId UpdateFlight
   * @returns {Response.model} 200 - flight info
   * @returns {Error.model} 404 - flight not found
   * @returns {Error.model} 400 - Operation failed
   * @security JWT
   */
  update(req, res, next) {
    const { updateFlight, flightsSerializer } = req;

    const { SUCCESS, ERROR, NOT_FOUND, BAD_REQUEST } = updateFlight.outputs;

    updateFlight
      .on(SUCCESS, (flight) => {
        res.status(Status.OK).json(flightsSerializer.serializeFlight(flight));
      })
      .on(NOT_FOUND, () => {
        res.status(Status.NOT_FOUND).json({
          type: 'NotFoundError',
        });
      })
      .on(BAD_REQUEST, () => {
        res.status(Status.BAD_REQUEST).json({
          type: 'InvalidParams',
        });
      })
      .on(ERROR, next);

    updateFlight.execute(req.params.id, req.body);
  },

  /**
   * The endpoint for deleting a flight
   * @route DELETE /flights/{id}
   * @param {string} id.path.required
   * @group flights - Operations about flights
   * @operationId DeleteFLight
   * @returns {Null} 200 - Operation successful
   * @returns {Error.model} 404 - flight not found
   * @security JWT
   */
  remove(req, res, next) {
    const { removeFlight } = req;

    const { SUCCESS, ERROR, NOT_FOUND } = removeFlight.outputs;

    removeFlight
      .on(SUCCESS, () => {
        res.status(Status.OK);
      })
      .on(NOT_FOUND, () => {
        res.status(Status.NOT_FOUND).json({
          type: 'NotFoundError',
        });
      })
      .on(ERROR, next);

    removeFlight.execute(req.params.id);
  },

  /**
   * The endpoint for searching flights
   * @route GET /flights/search
   * @param {string} name.query
   * @param {string} scheduledDate.query - ISO 8601 format (YYYY-MM-DDTHH:mm:ss.sssZ).
   * @param {string} departure.query
   * @param {string} destination.query
   * @param {integer} offset.query
   * @group flights - Operations about flights
   * @operationId SearchFlights
   * @returns {Response.model} 200 - An array of flights
   * @security JWT
   */
  search(req, res, next) {
    const { searchFlight, flightsSerializer } = req;
    const { offset, ...queryParams } = req.query;
    const { SUCCESS, ERROR } = searchFlight.outputs;

    searchFlight
      .on(SUCCESS, ({ flights, pageInfo }) => {
        res.status(Status.OK).json({
          flights: flightsSerializer.serializeSearch(flights),
          pageInfo,
        });
      })
      .on(ERROR, next);

    searchFlight.execute(queryParams, parseInt(offset));
  },
};

module.exports = FlightController;
