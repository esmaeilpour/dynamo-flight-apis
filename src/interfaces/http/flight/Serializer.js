const FlightsSerializer = {
  serializeFlight({
    id,
    name,
    number,
    scheduledDate,
    eta,
    departure,
    destination,
    fare,
    duration,
  }) {
    return {
      id,
      name,
      number,
      scheduledDate,
      eta,
      departure,
      destination,
      fare,
      duration,
    };
  },

  serializeSearch(results) {
    return results.map(this.serializeFlight);
  },
};

module.exports = FlightsSerializer;
