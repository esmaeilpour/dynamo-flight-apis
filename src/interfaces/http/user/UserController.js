const { Router } = require('express');
const { inject } = require('awilix-express');
const Status = require('http-status');

/**
 * @typedef Error
 * @property {string} type.required
 */

const UserController = {
  get router() {
    const router = Router();

    router.use(inject('userSerializer'));

    router.post('/login', inject('signIn'), this.login);

    router.post('/register', inject('signUp'), this.register);

    return router;
  },

  /**
   * The endpoing which be used for authenticating user
   * @route POST /users/login
   * @group users - Operations about users
   * @param {Credentials.model} Credentials.body.required
   * @operationId UserLogin
   * @returns {ResponseLogin.model} 200 - the user token
   * @returns {Error.model} 401 - Authentication failed
   */
  /**
   * @typedef ResponseLogin
   * @property {string} username
   * @property {string} token
   */
  /**
   * @typedef Credentials
   * @property {string} username.required - Some description for product
   * @property {string} password.required - Some description for product
   */
  login(req, res, next) {
    const { signIn, userSerializer } = req;

    const { SUCCESS, ERROR, NOT_FOUND } = signIn.outputs;

    signIn
      .on(SUCCESS, (user) => {
        res.status(Status.OK).json(userSerializer.serialize(user));
      })
      .on(NOT_FOUND, () => {
        res.status(Status.UNAUTHORIZED).json({
          type: 'Unauthorized',
        });
      })
      .on(ERROR, next);

    signIn.execute(req.body.username, req.body.password);
  },

  /**
   * The endpoint for registering new user
   * @route POST /users/register
   * @group users - Operations about users
   * @param {UserInfo.model} UserInfo.body.required
   * @operationId UserRegister
   * @returns {ResponseRegister.model} 200 - the new user info
   * @returns {Error.model} 400 - Bad request
   */
  /**
   * @typedef ResponseRegister
   * @property {string} username
   */
  /**
   * @typedef UserInfo
   * @property {string} username.required
   * @property {string} password.required
   */
  register(req, res, next) {
    const { signUp, userSerializer } = req;
    const { SUCCESS, ERROR, BAD_REQUEST, ALREADY_EXISTS } = signUp.outputs;

    signUp
      .on(SUCCESS, (user) => {
        res.status(Status.OK).json(userSerializer.serialize(user));
      })
      .on(ALREADY_EXISTS, () => {
        res.status(Status.BAD_REQUEST).json({
          type: 'AlreadyExists',
        });
      })
      .on(BAD_REQUEST, (error) => {
        res.status(Status.BAD_REQUEST).json({
          type: 'ValidationError',
          details: error.details,
        });
      })
      .on(ERROR, next);

    signUp.execute(req.body);
  },
};

module.exports = UserController;
