const UserSerializer = {
  serialize({ username, token }) {
    return {
      username,
      token
    };
  }
};

module.exports = UserSerializer;
