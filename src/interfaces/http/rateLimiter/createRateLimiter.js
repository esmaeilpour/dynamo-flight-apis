const rateLimit = require('express-rate-limit');

module.exports = function createRateLimiter({ windowMs, max, message = 'Too many requests, please try again later.' }) {
  return rateLimit({
    windowMs,
    max,
    message
  });
};
