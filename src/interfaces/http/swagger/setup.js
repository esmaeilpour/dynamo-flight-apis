const generator = require('express-swagger-generator');

module.exports = (app) => {
  const expressSwagger = generator(app);

  let options = {
    swaggerDefinition: {
      info: {
        title: 'Flights API',
        description: 'A REST API for providing clients with managing flights.',
        version: '1.0.0',
      },
      host: 'localhost:3000',
      basePath: '/v1',
      consumes: ['application/json'],
      produces: ['application/json'],
      schemes: ['http'],
      securityDefinitions: {
        JWT: {
          type: 'apiKey',
          in: 'header',
          name: 'Authorization',
          description: '',
        },
      },
    },
    basedir: __dirname, //app absolute path
    files: ['../**/*.js'], //Path to the API handle folder
  };
  expressSwagger(options);
};
