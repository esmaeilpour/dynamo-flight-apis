const Operation = require('src/app/Operation');

class RemoveFlight extends Operation {
  constructor({ flightRepository }) {
    super();
    this.repo = flightRepository;
  }

  async execute(id) {
    const { SUCCESS, NOT_FOUND, ERROR } = this.outputs;

    try {
      const numRemoved = await this.repo.removeFlight(id);
      if (!numRemoved) {
        this.emit(NOT_FOUND);
      } else {
        this.emit(SUCCESS);
      }
    } catch (error) {
      this.emit(ERROR, {
        details: error.message
      });
    }
  }
}

RemoveFlight.setOutputs(['SUCCESS', 'ERROR', 'NOT_FOUND']);

module.exports = RemoveFlight;
