const Operation = require('src/app/Operation');
const Flight = require('src/domain/flight/Flight');

class CreateFlight extends Operation {
  constructor({ flightRepository }) {
    super();
    this.repo = flightRepository;
  }

  async execute(flightData) {
    const { SUCCESS, BAD_REQUEST, ERROR } = this.outputs;
    const flight = new Flight(flightData);

    try {
      const { valid, errors } = flight.validate();
      if (!valid) {
        const error = new Error('ValidationError');
        error.details = errors;
        throw error;
      }

      const newFlight = await this.repo.createFlight(flight);
      this.emit(SUCCESS, newFlight);
    } catch (error) {
      if (error.message === 'ValidationError') {
        return this.emit(BAD_REQUEST, error);
      }

      this.emit(ERROR, {
        details: error.message,
      });
    }
  }
}

CreateFlight.setOutputs(['SUCCESS', 'ERROR', 'BAD_REQUEST']);

module.exports = CreateFlight;
