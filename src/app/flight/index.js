module.exports = {
  CreateFlight: require('./CreateFlight'),
  UpdateFlight: require('./UpdateFlight'),
  RemoveFlight: require('./RemoveFlight'),
  SearchFlights: require('./SearchFlights'),
};
