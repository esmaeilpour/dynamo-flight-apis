const Operation = require('src/app/Operation');
const Flight = require('src/domain/flight/Flight');

class UpdateFlight extends Operation {
  constructor({ flightRepository }) {
    super();
    this.repo = flightRepository;
  }

  async execute(id, flightData) {
    const { SUCCESS, BAD_REQUEST, NOT_FOUND, ERROR } = this.outputs;
    const flight = new Flight(flightData);

    try {
      const { valid, errors } = flight.validate();
      if (!valid) {
        const error = new Error('ValidationError');
        error.details = errors;
        throw error;
      }

      const updatedDoc = await this.repo.updateFlight(id, flight);
      if (updatedDoc) {
        this.emit(SUCCESS, updatedDoc);
      } else {
        this.emit(NOT_FOUND);
      }
    } catch (error) {
      if (error.message === 'ValidationError') {
        return this.emit(BAD_REQUEST, error);
      }

      this.emit(ERROR, {
        details: error.message,
      });
    }
  }
}

UpdateFlight.setOutputs(['SUCCESS', 'ERROR', 'NOT_FOUND', 'BAD_REQUEST']);

module.exports = UpdateFlight;
