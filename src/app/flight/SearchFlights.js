const Operation = require('src/app/Operation');
// const Flight = require('src/domain/flight/Flight');

class SearchFlights extends Operation {
  constructor({ flightRepository, config }) {
    super();
    this.repo = flightRepository;
    this.config = config;
  }

  async execute(queryParams, offset = 0) {
    const { SUCCESS, ERROR } = this.outputs;
    const perPage = this.config.pagination.perPage;

    try {
      const total = await this.repo.countFlights(queryParams);
      const pageInfo = {
        perPage,
        offset,
        total,
      };
      const flights = await this.repo.searchFlights(queryParams, offset, perPage);
      this.emit(SUCCESS, { flights, pageInfo });
    } catch (error) {
      this.emit(ERROR, {
        details: error.message,
      });
    }
  }
}

SearchFlights.setOutputs(['SUCCESS', 'ERROR']);

module.exports = SearchFlights;
