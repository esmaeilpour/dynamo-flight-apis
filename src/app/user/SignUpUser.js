const Operation = require('src/app/Operation');
const User = require('src/domain/user/User');

class SignUpUser extends Operation {
  constructor({ userRepository }) {
    super();
    this.repo = userRepository;
  }

  async execute(userData) {
    const { SUCCESS, BAD_REQUEST, ERROR, ALREADY_EXISTS } = this.outputs;
    const user = new User(userData);

    try {
      const { valid, errors } = user.validate();

      if (!valid) {
        const error = new Error('ValidationError');
        error.details = errors;
        throw error;
      }

      if (!user.isStrongPassword()) {
        const error = new Error('ValidationError');
        error.details = 'password is not strong';
        throw error;
      }

      const newUser = await this.repo.addUser(user);
      this.emit(SUCCESS, newUser);
    } catch (error) {
      if (error.errorType === 'uniqueViolated') {
        return this.emit(ALREADY_EXISTS);
      }

      if (error.message === 'ValidationError') {
        return this.emit(BAD_REQUEST, error);
      }

      this.emit(ERROR, {
        details: error.message,
      });
    }
  }
}

SignUpUser.setOutputs(['SUCCESS', 'ERROR', 'BAD_REQUEST', 'ALREADY_EXISTS']);

module.exports = SignUpUser;
