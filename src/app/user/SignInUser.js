const Operation = require('src/app/Operation');
const jwt = require('jsonwebtoken');

class SignInUser extends Operation {
  constructor({ userRepository, config }) {
    super();
    this.config = config;
    this.repo = userRepository;
  }

  async execute(username, password) {
    const { SUCCESS, NOT_FOUND, ERROR } = this.outputs;

    try {
      const user = await this.repo.findByUsername(username);
      if (!user) {
        throw new Error('AuthenticationFailed');
      }
      if (user.isPasswordMatched(password) === false) {
        throw new Error('AuthenticationFailed');
      }

      const token = jwt.sign({ id: user._id }, this.config.jwt.secret, {
        algorithm: this.config.jwt.algorithm,
      });
      user.setToken(token);
      this.emit(SUCCESS, user);

    } catch (error) {
      if (error.message === 'AuthenticationFailed') {
        return this.emit(NOT_FOUND, error);
      }
      this.emit(ERROR, {
        details: error.message,
      });
    }
  }
}

SignInUser.setOutputs(['SUCCESS', 'ERROR', 'NOT_FOUND']);

module.exports = SignInUser;
