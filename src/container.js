const { createContainer, asClass, asFunction, asValue } = require('awilix');
const { scopePerRequest } = require('awilix-express');

const config = require('../config');
const Application = require('./app/Application');

const {
  CreateFlight,
  UpdateFlight,
  RemoveFlight,
  SearchFlights,
} = require('./app/flight');

const { SignUpUser, SignInUser } = require('./app/user');

const UserSerializer = require('./interfaces/http/user/Serializer');
const FlightSerializer = require('./interfaces/http/flight/Serializer');

const Server = require('./interfaces/http/Server');
const router = require('./interfaces/http/router');
const loggerMiddleware = require('./interfaces/http/logging/loggerMiddleware');
const errorHandler = require('./interfaces/http/errors/errorHandler');
const devErrorHandler = require('./interfaces/http/errors/devErrorHandler');
const createRateLimiter = require('./interfaces/http/rateLimiter/createRateLimiter');
const swaggerSetup = require('./interfaces/http/swagger/setup');
const logger = require('./infra/logging/logger');

const FlightRepository = require('./infra/flight/InMemoryRepository');
const UserRepository = require('./infra/user/InMemoryRepository');

const InMemDB = require('./infra/support/InMemoryDB');

const container = createContainer();

// System
container
  .register({
    app: asClass(Application).singleton(),
    server: asClass(Server).singleton(),
  })
  .register({
    router: asFunction(router).singleton(),
    logger: asFunction(logger).singleton(),
  })
  .register({
    config: asValue(config),
  });

// Middlewares
container
  .register({
    loggerMiddleware: asFunction(loggerMiddleware).singleton(),
  })
  .register({
    containerMiddleware: asValue(scopePerRequest(container)),
    errorHandler: asValue(config.production ? errorHandler : devErrorHandler),
    rateLimiter: asValue(createRateLimiter),
    swagger: asValue(swaggerSetup),
  });

// Repositories
container.register({
  flightRepository: asClass(FlightRepository).singleton(),
  userRepository: asClass(UserRepository).singleton(),
});

// Third-party Services
container.register({});

// Operations
container.register({
  // Users
  signIn: asClass(SignInUser),
  signUp: asClass(SignUpUser),
  // Flights
  createFlight: asClass(CreateFlight),
  updateFlight: asClass(UpdateFlight),
  removeFlight: asClass(RemoveFlight),
  searchFlight: asClass(SearchFlights),
});

// Serializers
container.register({
  userSerializer: asValue(UserSerializer),
  flightsSerializer: asValue(FlightSerializer),
});

// Helpers
container.register({
  memoryDB: asValue(InMemDB),
});

module.exports = container;
