module.exports = {
  web: {
    port: process.env.PORT
  },
  logging: {
    appenders: [
      { type: 'console', layout: { type: 'basic' } }
    ]
  },
  rate_limit: {
    windowMs: 1 * 60 * 1000, // 1 minutes
    max: 10
  },
  pagination: {
    perPage: 10,
  }
};
