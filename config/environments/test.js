module.exports = {
  web: {},
  rate_limit: {
    windowMs: 1 * 60 * 1000, // 1 minutes
    max: 1000
  },
  jwt: {
    secret: 'foobar'
  },
  pagination: {
    perPage: 10,
  }
};
