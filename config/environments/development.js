const path = require('path');
const logPath = path.join(__dirname, '../../logs/development.log');

module.exports = {
  web: {
    port: 3000
  },
  logging: {
    appenders: [
      { type: 'console' },
      { type: 'file', filename: logPath }
    ]
  },
  rate_limit: {
    windowMs: 1 * 60 * 1000, // 1 minutes
    max: 10
  },
  jwt: {
    secret: 'foobar',
    algorithm: 'HS256'
  },
  pagination: {
    perPage: 10,
  }
};
