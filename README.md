#### Forked from <a href="https://github.com/talyssonoc/node-api-boilerplate">talyssonoc/node-api-boilerplate</a>

# Flight API
A Node web APIs inspired by DDD and Clean Architecture focused on separation of concerns and scalability.

## Aditional Features

<dl>
  <dt>Express Rate Limit</dt>
  <dd>
    <a href="https://github.com/nfriedly/express-rate-limit">Express Rate Limit</a> is a basic rate-limiting middleware for Express. Use to limit repeated requests to public APIs and/or endpoints.
  </dd>
  <dt>Express Swagger Generator</dt>
  <dd>
    <a href="https://github.com/pgroot/express-swagger-generator">Express Swagger Generator</a> is a middleware to generate and show swagger-ui for your local (or external) api.
  </dd>
  <dt>Sinon JS</dt>
  <dd>
  <a href="https://github.com/sinonjs/sinon">Sinon JS</a> is a popular JavaScript library that lets you replace complicated parts of your code that are hard to test for “placeholders,” so you can keep your unit tests fast and deterministic, as they should be.
  </dd>
</dl>

## Quick start

1. Install the dependencies with `yarn` (click here if [you don't have Yarn installed](https://yarnpkg.com/docs/install))

2. Run the application in development mode with `npm run dev`
3. Access `http://localhost:3000/api-docs` and you're ready to go!

## Scripts

This boilerplate comes with a collection of npm scripts to make your life easier, you'll run them with `npm run <script name>` or `yarn run <script name>`:

- `dev`: Run the application in development mode
- `start`: Run the application in production mode (prefer not to do that in development) 
- `test`: Run the test suite
- `test:unit`: Run only the unit tests
- `test:features`: Run only the features tests
- `coverage`: Run only the unit tests and generate code coverage for them, the output will be on `coverage` folder
- `lint`: Lint the codebase
- `console`: Open the built-in console, you can access the DI container through the `container` variable once it's open, the console is promise-friendly. Click [here](https://github.com/talyssonoc/node-api-boilerplate/wiki/Application-console) to know more about the built-in console

## TODO
- [typescript](https://www.typescriptlang.org/) integrate with typescript
- [dataloader](https://github.com/graphql/dataloader) batch finding users for reducing db calls
- [graphql](https://graphql.org/graphql-js/) Add graphql interface
- [paginnation connection](https://www.apollographql.com/blog/explaining-graphql-connections-c48b7c3d6976/) Use this approach to improve pagination experience
- 100% test coverage :)
 

## Other Tech

- [Node v7.6+](http://nodejs.org/)
- [Express](https://npmjs.com/package/express)
- [Awilix](https://www.npmjs.com/package/awilix)
- [Structure](https://www.npmjs.com/package/structure)
- [HTTP Status](https://www.npmjs.com/package/http-status)
- [Log4js](https://www.npmjs.com/package/log4js)
- [Morgan](https://www.npmjs.com/package/morgan)
- [Express Status Monitor](https://www.npmjs.com/package/express-status-monitor)
- [Nodemon](https://www.npmjs.com/package/nodemon)
- [PM2](https://www.npmjs.com/package/pm2)
- [Mocha](https://www.npmjs.com/package/mocha)
- [Chai](https://www.npmjs.com/package/chai)
- [Istanbul](https://www.npmjs.com/package/istanbul) + [NYC](https://www.npmjs.com/package/nyc)
- [ESLint](https://www.npmjs.com/package/eslint)